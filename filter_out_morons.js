/*
  Modifies DOM if there is a match between a client's IP & a table of
  morons. ES5, IE11+.

  Usage:

  <script src="filter_out_morons.js"></script>
  <script>
    // or leave it our & use ?fom-test-cc=RU in the URL instead (chrome/firefox)
    filter_out_morons.test = 'RU'
    filter_out_morons.run()
  </script>

*/

var filter_out_morons = (function () {
    'use strict';

    var module = {}
    module.test = typeof URLSearchParams === "function"
	? new URLSearchParams(location.search.slice(1)).get('fom-test-cc')
	: null
    var log = console.log.bind(console, 'filter_out_morons:')

    // a partial list of UN members that "recognized" the Crimea as
    // part of Russia
    module.table = {
	'RU': {
	    en: { demonym: 'Russian' },
	    uk: { demonym: 'Росіянин' }
	},
	'KG': {
	    en: { demonym: 'Kyrgyz' },
	    uk: { demonym: 'Киргизстанець' }
	},
	'NI': {
	    en: { demonym: 'Nicaraguan' },
	    uk: { demonym: 'Нікарагуанець' }
	}
    }

    module.providers = {
	'freegeoip': {		// blocked by ublock origin
	    url: 'https://freegeoip.net/json/',
	    cc: function(json) { return json.country_code }
	},
	'ipinfo': {		// blocked by ublock origin
	    url: 'https://ipinfo.io/json/',
	    cc: function(json) { return json.country }
	},
	'ip.nf': {
	    url: 'https://ip.nf/me.json',
	    cc: function(json) { return json.ip.country_code }
	},
	'instant.cm': {
	    url: 'https://api.instantcm.com/api/v1/geo-ip',
	    cc: function(json) { return json.alpha2 }
	}
    }

    var popup = function(msg) {
	var glass = document.createElement('div')
	glass.style.position = 'fixed'
	glass.style.zIndex = '1050'
	glass.style.left = '0'
	glass.style.top = '0'
	glass.style.width = '100%'
	glass.style.height = '100%'
	glass.style.overflow = 'auto'
	glass.style.backgroundColor = 'rgb(0,0,0)'
	glass.style.backgroundColor = 'rgba(0,0,0,0.4)'

	var dlg = document.createElement('div')
	dlg.style.padding = '0 1em 0 1em'
	dlg.style.fontSize = 'x-large'
	dlg.style.textAlign = 'left'
	dlg.style.backgroundColor = '#ee0000'
	dlg.style.color = '#fcfcfc'
	dlg.style.boxShadow = '3px 3px 3px rgba(0, 0, 0, 0.8)'
	dlg.style.position = 'fixed'
	dlg.style.top = '50%'
	dlg.style.left = '50%'
	dlg.style.marginRight = '-50%'
	dlg.style.transform = 'translate(-50%, -50%)'

	document.body.appendChild(glass).appendChild(dlg)
	dlg.innerHTML = ['<div><p>', msg, '</p></div>'].join("\n")

	var links = dlg.querySelectorAll('a')
	for (var idx = 0; idx < links.length; ++idx)
	    links[idx].style.color = 'yellow'
    }

    var say_hello = function(data) {
	var msg = [
	    "I am sorry to inform you but no " + data.en.demonym + " is welcome here.",
	    "<br>",
	    "Why? See <a href='https://en.wikipedia.org/wiki/Russian_military_intervention_in_Ukraine_(2014%E2%80%93present)'>Russian military intervention in Ukraine</a>.",
	    "<br>",
	    "<br>",
	    "(Переклад для лінгвоінвалідів)",
	    "<br>",
	    "<b>" + data.uk.demonym + ", блядь? Нахуй пішов.</b>"
	].join("\n");

	popup(msg)

	// disable scrolling
	document.body.style.height = '100%'
	document.body.style.overflow = 'hidden'
    }

    var ipcheck = function(provider_name, provider, cb) {
	var log = console.log.bind(console, 'filter_out_morons: ' + provider_name + ':')

	var req = new XMLHttpRequest();
	req.addEventListener("load", function() {
	    var r
	    try {
		r = provider.cc(JSON.parse(this.responseText))
		if (!r) throw new Error('json data error')
	    } catch (err) {
		cb(err)
		return
	    }
	    if (module.test) r = module.test

	    log("country code = " + r)
	    if (r in module.table) {
		log("look who we've got here")
		say_hello(module.table[r])
		cb(null, true)
		return
	    }

	    log("no match")
	})
	req.addEventListener('loadstart', log('check...'))
	req.addEventListener('error', function() {
	    cb(new Error('request failed'))
	})
	req.open("GET", provider.url)
	req.send()
    }

    // Sequentially iterate through `module.providers`. We cannot use
    // a simple loop, for every req is async; we cannot use Promises,
    // for we're targeting ES5.
    module.run = function(index) {
	index = index || 0

	var provider = Object.keys(module.providers)[index]
	if (!provider) {
	    log('no more geo providers :(')
	    return
	}

	ipcheck(provider, module.providers[provider], function(err, _) {
	    if (!err) return
	    log(err)
	    // Recursion!
	    module.run(index + 1)
	})
    }

    return module

})();
